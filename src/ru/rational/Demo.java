package ru.rational;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Демонстрационый класс для предствавления программы
 *
 * @author Дунькин В.
 */
public class Demo {
    public static void main(String[] args) {
        StringBuilder expression = Input();

        String resultExpression = expression + " = ";

        expression = convertFromMixedToWrong(expression);
        expression = convertArithmeticOperators(expression, "\\+\\s-", "- ");
        expression = convertArithmeticOperators(expression, "-\\s-", "+ ");

        expression = decision(expression, "*",":");
        expression = decision(expression, "+", "-");

        resultExpression += expression;
        System.out.println(resultExpression);
    }

    private static StringBuilder Input() {
        Scanner reader = new Scanner(System.in);
        String exp;

        do {
            System.out.print("Введите выражение: ");
            exp = reader.nextLine();

            if (isZero(exp)) {
                System.out.println("Ошибка! В знаменателе стоит ноль! ");
                Input();
            }
        } while (!isFraction(exp) && !isOperator(exp));

        return new StringBuilder(exp);
    }

    private static Boolean isZero(String exp) {
        Pattern p = Pattern.compile("\\d+/0");
        Matcher m = p.matcher(exp);
        return m.find();
    }

    private static Boolean isFraction(String exp) {
        Boolean flag;
        String[] arrFractions = exp.split("\\s[*+-:]\\s");

        int countElements = arrFractions.length;
        int count = 0;

        for (String fraction : arrFractions) {
            if (Pattern.matches("(-)?\\d+/\\d+", fraction)) {
                count++;
            }
            if (Pattern.matches("(-)?\\d+\\(\\d+/\\d+\\)", fraction)) {
                count++;
            }
            if (Pattern.matches("(-)?\\d+", fraction)) {
                count++;
            }
        }

        flag = count == countElements;
        return flag;
    }

    private static Boolean isOperator(String exp) {
        Boolean flag;
        String[] arrFractions = exp.split("(\\s)?(-)?\\d+(/\\d+|\\(\\d+/\\d+\\))?(\\s)?");

        int countElements = arrFractions.length;
        int count = 0;

        for (String fraction : arrFractions) {
            if (Pattern.matches("[*+-:]", fraction)) {
                count++;
            }
            if (Pattern.matches("/", fraction)) {
                count--;
            }
        }

        flag = count == countElements;
        return flag;
    }

    private static StringBuilder convertFromMixedToWrong(StringBuilder exp) {
        String expression = exp.toString();

        Pattern p = Pattern.compile("(-)?\\d+\\(\\d+/\\d+\\)");
        Matcher m = p.matcher(expression);

        String[] number;
        String fraction;
        int num;
        int denum;

        while (m.find()) {
            number = m.group().replaceAll("\\(|/|\\)", " ").split(" ");

            num = (Integer.valueOf(number[0]) * Integer.valueOf(number[2])) + Integer.valueOf(number[1]);
            denum = Integer.valueOf(number[2]);

            fraction = String.valueOf(num) + "/" + String.valueOf(denum);
            expression = expression.replace(m.group(), fraction);
        }

        return new StringBuilder(expression);
    }

    private static StringBuilder convertArithmeticOperators(StringBuilder exp, String regexp, String oper) {
        String expression = exp.toString();

        Pattern p = Pattern.compile(regexp);
        Matcher m = p.matcher(expression);

        while (m.find()) {
            expression = expression.replace(m.group(), oper);
        }

        return new StringBuilder(expression);
    }

    private static StringBuilder decision(StringBuilder exp, String opOne, String opTwo) {
        String expression = exp.toString();
        String oper = String.format("[%s%s]", opOne, opTwo);
        String regexp = "\\d+(/\\d+)?\\s" + oper + "\\s\\d+(/\\d+)?";
        String[] expFromFrac;
        String[] fraction;
        Rational f1;
        Rational f2;

        do {
            Matcher m = Pattern.compile(regexp).matcher(expression);

            while (m.find()) {
                int index = 0;
                if (m.group().contains(opOne)){
                     index = m.group().indexOf(opOne);
                }
                if (m.group().contains(opTwo)){
                    index = m.group().indexOf(opTwo);
                }
                String operation = String.valueOf(m.group().charAt(index));

                expFromFrac = m.group().split("\\s" + oper + "\\s");

                fraction = expFromFrac[0].split("/");
                if (fraction.length == 2) {
                    f1 = new Rational(Integer.valueOf(fraction[0]), Integer.valueOf(fraction[1]));
                } else {
                    f1 = new Rational(Integer.valueOf(fraction[0]));
                }

                fraction = expFromFrac[1].split("/");
                if (fraction.length == 2) {
                    f2 = new Rational(Integer.valueOf(fraction[0]), Integer.valueOf(fraction[1]));
                } else {
                    f2 = new Rational(Integer.valueOf(fraction[0]));
                }

                switch (operation) {
                    case "*":
                        f1.Multiplication(f2);
                        f1.Reduction();
                        break;
                    case ":":
                        f1.Division(f2);
                        f1.Reduction();
                        break;
                    case "+":
                        f1.Addition(f2);
                        f1.Reduction();
                        break;
                    case "-":
                        f1.Subtraction(f2);
                        f1.Reduction();
                        break;
                }

                expression = expression.replace(m.group(), String.valueOf(f1));
                System.out.println(expression);
            }

            if(Pattern.matches("-\\d+/\\d+",expression)){
                break;
            }
        } while (expression.contains(opOne) || expression.contains(opTwo));

        return new StringBuilder(expression);
    }
}
