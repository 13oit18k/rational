package ru.rational;

/**
 * Класс для описания дроби и операций над ними
 *
 * @author Борисова А., Поляков Д.
 */
public class Rational {
    private int numerator;// числитель
    private int denominator; // знаменатель

    /**
     * Конструктор инициализирует дробь вида:
     * 1/1
     */
    public Rational() {
        this.numerator = 1;
        this.denominator = 1;
    }

    /**
     * Констуктор инициализирует дроби вида:
     * numerator/1
     *
     * @param numerator Числитель
     */
    public Rational(int numerator) {
        this.numerator = numerator;
        this.denominator = 1;
    }

    /**
     * Констуктор с параметрами с явной проверкой на ноль
     *
     * @param numerator   Числитель
     * @param denominator - Знаменатель
     */
    public Rational(int numerator, int denominator) {
        if (denominator == 0) {
            throw new ArithmeticException("Знаменатель не может быть равен нулю");
        }
        this.numerator = numerator;
        this.denominator = denominator;
    }

    /**
     * Геттер числителя
     *
     * @return числитель
     */
    int getNumerator() {
        return numerator;
    }

    /**
     * Сеттер числителя
     *
     * @param numerator числитель
     */
    void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    /**
     * Геттер знаменателя
     *
     * @return знаменатель
     */
    int getDenominator() {
        return denominator;
    }

    /**
     * Сеттер знаменателя
     *
     * @param denominator знаменатель
     */
    void setDenominator(int denominator) {
        if (denominator == 0) {
            throw new ArithmeticException("Знаменатель не может быть равен нулю");
        }
        this.denominator = denominator;
    }

    /**
     * Вывод дроби
     *
     * @return String numerator/denominator
     */
    @Override
    public String toString() {
        return numerator + "/" + denominator;
    }

    /**
     * Метод, для подсчета умножения двух дробей
     *
     * @param fraction - Объект дроби в качестве множителя
     */
    void Multiplication(Rational fraction) {
        this.numerator = this.numerator * fraction.numerator;
        this.denominator = this.denominator * fraction.denominator;
    }

    /**
     * Метод, для подсчета деления двух дробей
     *
     * @param fraction - Объект дроби в качестве делителя
     */
    void Division(Rational fraction) {
        if (fraction.numerator == 0) {
            throw new ArithmeticException("Числитель делителя равен 0..");
        }

        this.numerator = this.numerator * fraction.denominator;
        this.denominator = this.denominator * fraction.numerator;
    }

    /**
     * Метод, для подсчета сложения двух дробей
     *
     * @param fraction - Объект дроби в качестве слагаемого
     */
    void Addition(Rational fraction) {
        this.numerator = (this.numerator * fraction.denominator) + (this.denominator * fraction.numerator);
        this.denominator = this.denominator * fraction.denominator;
    }

    /**
     * Метод, для подсчета вычитания двух дробей
     *
     * @param fraction - Объект дроби в качестве вычитаемого
     */
    void Subtraction(Rational fraction) {
        this.numerator = (this.numerator * fraction.denominator) - (this.denominator * fraction.numerator);
        this.denominator = this.denominator * fraction.denominator;
    }

    /**
     * Метод, для сокращения дроби к меньшим значениям
     */
    void Reduction() {
        Boolean flag = false;

        if (this.numerator < 0) {
            this.numerator = Math.abs(this.numerator);
            flag = true;
        }
        if (this.denominator < 0) {
            this.denominator = Math.abs(this.denominator);
            flag = true;
        }


        for (int i = 2; i < this.numerator; i++) {
            while((this.numerator % i == 0) && (this.denominator % i == 0)) {
                this.numerator = this.numerator / i;
                this.denominator = this.denominator / i;
            }
        }

        if (flag){
            this.numerator = -this.numerator;
        }
    }
}