package ru.rational;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Класс с тестами
 */
public class RationalTest {
    @Test
    public void testRational() {
        Rational f = new Rational();

        assertEquals("1/1", String.valueOf(f));
    }

    @Test
    public void testRationalTwo() {
        Rational f = new Rational(6);

        assertEquals("6/1", String.valueOf(f));
    }

    @Test
    public void testRationalThird() {
        Rational f = new Rational(6, 3);

        assertEquals("6/3", String.valueOf(f));
    }

    @Test(expected = ArithmeticException.class)
    public void testRationalThirdZero() {
        Rational f = new Rational(6, 0);

        assertEquals("6/0", String.valueOf(f));
    }

    @Test
    public void testMultiplicationGet() {
        Rational f1 = new Rational(4, 5);
        Rational f2 = new Rational(5, 6);

        f1.Multiplication(f2);

        assertEquals(20, f1.getNumerator());
        assertEquals(30, f1.getDenominator());
    }

    @Test
    public void testMultiplicationToString() {
        Rational f1 = new Rational(4, 5);
        Rational f2 = new Rational(5, 6);

        f1.Multiplication(f2);

        assertEquals("20/30", String.valueOf(f1));
    }

    @Test(expected = ArithmeticException.class)
    public void testMultiplicationZero() {
        Rational f1 = new Rational(4, 4);
        Rational f2 = new Rational(5, 0);

        f1.Multiplication(f2);

        assertEquals(20, f1.getNumerator());
        assertEquals(30, f1.getDenominator());
    }

    @Test
    public void testDivision() {
        Rational f1 = new Rational(10, 7);
        Rational f2 = new Rational(15, 3);

        f1.Division(f2);

        assertEquals(30, f1.getNumerator());
        assertEquals(105, f1.getDenominator());
    }

    @Test(expected = ArithmeticException.class)
    public void testDivisionZero() {
        Rational f1 = new Rational(10, 0);
        Rational f2 = new Rational(15, 3);

        f1.Division(f2);

        assertEquals(30, f1.getNumerator());
        assertEquals(105, f1.getDenominator());
    }

    @Test(expected = ArithmeticException.class)
    public void testDivisionZeroTwo() {
        Rational f1 = new Rational(10, 7);
        Rational f2 = new Rational(0, 3);

        f1.Division(f2);

        assertEquals(30, f1.getNumerator());
        assertEquals(105, f1.getDenominator());
    }

    @Test
    public void testAddition() {
        Rational f1 = new Rational(3, 4);
        Rational f2 = new Rational(6, 8);

        f1.Addition(f2);

        assertEquals(48, f1.getNumerator());
        assertEquals(32, f1.getDenominator());
    }

    @Test(expected = ArithmeticException.class)
    public void testAdditionZero() {
        Rational f1 = new Rational(3, 0);
        Rational f2 = new Rational(6, 8);

        f1.Addition(f2);

        assertEquals(18, f1.getNumerator());
        assertEquals(0, f1.getDenominator());
    }

    @Test
    public void testAdditionZeroTwo() {
        Rational f1 = new Rational(3, 4);
        Rational f2 = new Rational(0, 8);

        f1.Addition(f2);

        assertEquals(24, f1.getNumerator());
        assertEquals(32, f1.getDenominator());
    }

    @Test
    public void testSubtraction() {
        Rational f1 = new Rational(5, 7);
        Rational f2 = new Rational(4, 3);

        f1.Subtraction(f2);

        assertEquals(-13, f1.getNumerator());
        assertEquals(21, f1.getDenominator());
    }

    @Test(expected = ArithmeticException.class)
    public void testSubtractionZero() {
        Rational f1 = new Rational(5, 0);
        Rational f2 = new Rational(4, 3);

        f1.Subtraction(f2);

        assertEquals(-13, f1.getNumerator());
        assertEquals(21, f1.getDenominator());
    }

    @Test
    public void testReduction() {
        Rational f = new Rational(35, 15);

        f.Reduction();

        assertEquals("7/3", String.valueOf(f));
    }

    @Test
    public void testReductionTwo() {
        Rational f = new Rational(100, 25);

        f.Reduction();

        assertEquals("4/1", String.valueOf(f));
    }
}